import { Injectable } from '@angular/core';
import { Producto } from '../models/Producto';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  productos: Producto[];
  constructor() {
    this.productos = [];
   }

   getProductos() {
     if (localStorage.getItem('productos') === null) {
       return this.getProductos;
     } else {
       this.productos = JSON.parse(localStorage.getItem('productos'));
       return this.productos;
     }
   }
}
