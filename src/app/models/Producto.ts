export interface Producto {
    id: number;
    nombre: string;
    precio: number;
    cantidad_disponible: number;
}
