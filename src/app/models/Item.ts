export interface Item {
    id: number;
    cantidad_elegida: number;
    subtotal: number;
    id_producto?: number;
    id_compra?: number;
}
